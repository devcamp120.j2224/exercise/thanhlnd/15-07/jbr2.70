
import java.util.Date;

public class Visit {
    Customer customer;
    Date date;
    double serviceExpense;
    double productExpense;

    public Visit() {
        super();
    }

    public Visit(String name, Date date) {
        this.customer = new Customer(name);
        this.date = date;
    }

    // public Visit(Customer customer,Date date, double serviceExpense, double productExpense) {
    //     this.customer = customer;
    //     this.date =  date;
    //     this.serviceExpense = serviceExpense;
    //     this.productExpense = productExpense;
    // }

    public Visit(Customer customer, Date date, int serviceExpense, int productExpense) {
        this.customer = customer;
        this.date =  date;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getServiceExpense() {
        return serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    // phương thức tính tổng chi phí dịch vụ
    public double getCost() {
        return this.serviceExpense + this.productExpense;
    }

    @Override
    public String toString() {
        return "Visit [customer=" + customer + ", date=" + date + ", productExpense=" + this.productExpense
                + ", serviceExpense=" + this.serviceExpense + ", getCost" + getCost() + "]";
    }

}
